package net.twerion.coinshop.listener;

import net.twerion.coinshop.Coinshop;
import net.twerion.coinshop.mysql.statement.Statement;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.sql.PreparedStatement;

public class PlayerJoin implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        Coinshop.getMysqlManager().executeAsync(() -> {
            try {
                PreparedStatement preparedStatement = Statement.updateName(player.getUniqueId().toString(), player.getName());
                Coinshop.getMysqlManager().getMysql()
                        .queryUpdate(preparedStatement);
                preparedStatement.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
