package net.twerion.coinshop.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.twerion.coinshop.api.ICoinManager;
import net.twerion.coinshop.mysql.MysqlManager;
import net.twerion.coinshop.mysql.statement.Statement;

import java.sql.PreparedStatement;
import java.util.function.Consumer;

@AllArgsConstructor
public class CoinManager implements ICoinManager {

    @Getter private MysqlManager mysqlManager;

    @Override
    public void addCoins(String uuid, int amount) {
        this.update(uuid, amount, ChangeType.ADD);
    }

    @Override
    public void removeCoins(String uuid, int amount) {
        this.update(uuid, amount, ChangeType.REMOVE);
    }

    @Override
    public void setCoins(String uuid, int amount) {
        this.update(uuid, amount, ChangeType.SET);
    }

    @Override
    public void loadCoins(String uuid, Consumer<Integer> consumer) {
        this.mysqlManager.executeAsync(() -> {
            try {
                consumer.accept(this.mysqlManager.getCache().get(uuid));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void update(String uuid, int amount, ChangeType changeType) {
        this.loadCoins(uuid, coins -> this.mysqlManager.executeAsync(() -> {
            try {
                int updatedCoins = coins;
                switch (changeType) {
                    case ADD:
                        updatedCoins += amount;
                        break;
                    case REMOVE:
                        updatedCoins -= amount;
                        break;
                    case SET:
                        updatedCoins = coins;
                        break;
                }
                this.mysqlManager.getCache().put(uuid, updatedCoins);

                PreparedStatement preparedStatement = Statement.updateCoins(uuid, updatedCoins);
                this.mysqlManager.getMysql().queryUpdate(preparedStatement);
                preparedStatement.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));
    }

    private enum ChangeType {
        ADD,
        REMOVE,
        SET
    }

}
