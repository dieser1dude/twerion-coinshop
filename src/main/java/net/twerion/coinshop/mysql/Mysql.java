package net.twerion.coinshop.mysql;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@RequiredArgsConstructor
public class Mysql {

    @NonNull @Getter private String host;
    @NonNull @Getter private int port;
    @NonNull @Getter private String user;
    @NonNull @Getter private String password;
    @NonNull @Getter private String database;
    @Getter private Connection connection;

    public Connection openConnection() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");

        return this.connection = DriverManager.getConnection("jdbc:mysql://" +
                this.host + ":" +
                this.port + "/" +
                this.database, this.user, this.password);
    }

    public void closeConnection() throws Exception {
        if (this.connection != null) {
            this.connection.close();
            this.connection = null;
        }
    }

    @SneakyThrows
    public boolean connectionIsActive() {
        return this.connection != null && this.connection.isValid(10) && !this.connection.isClosed();
    }

    @SneakyThrows
    public void updateConnection() {
        if (!this.connectionIsActive()) {
            this.openConnection();
        }
    }

    public PreparedStatement prepare(String query) throws Exception {
        this.updateConnection();
        return this.connection.prepareStatement(query);
    }

    public ResultSet query(String query) throws Exception {
        return this.query(this.prepare(query));
    }

    public ResultSet query(PreparedStatement preparedStatement) throws Exception {
        this.updateConnection();
        return preparedStatement.executeQuery();
    }

    public void queryUpdate(String query) throws Exception {
        this.queryUpdate(this.prepare(query));
    }

    public void queryUpdate(PreparedStatement preparedStatement) throws Exception {
        this.updateConnection();
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

}
