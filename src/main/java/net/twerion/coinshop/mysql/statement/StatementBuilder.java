package net.twerion.coinshop.mysql.statement;

import net.twerion.coinshop.Coinshop;

import java.sql.PreparedStatement;

public class StatementBuilder {

    private PreparedStatement preparedStatement;
    private int index = 1;

    public StatementBuilder(String query) throws Exception {
        this.preparedStatement = Coinshop.getMysqlManager().getMysql().prepare(query);
    }

    public StatementBuilder string(String value) throws Exception {
        this.preparedStatement.setString(this.index++, value);
        return this;
    }

    public StatementBuilder integer(int value) throws Exception {
        this.preparedStatement.setInt(this.index++, value);
        return this;
    }

    public PreparedStatement build() {
        return this.preparedStatement;
    }


}
