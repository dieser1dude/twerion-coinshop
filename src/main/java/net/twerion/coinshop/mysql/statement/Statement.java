package net.twerion.coinshop.mysql.statement;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.sql.PreparedStatement;

@AllArgsConstructor
public enum Statement {

    UPDATE_TABLE("CREATE TABLE IF NOT EXISTS `coins` (`uuid` VARCHAR(36) PRIMARY KEY, `name` VARCHAR(16), `coins` INT DEFAULT 0)"),
    UPDATE_NAME("INSERT INTO `coins` (`uuid`, `name`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `name` = ?;"),
    UPDATE_COINS("UPDATE `coins` SET `coins` = ? WHERE `uuid` = ?;"),
    GET_COINS("SELECT * FROM `coins` WHERE `uuid` = ?;");

    @Getter private String query;

    public static PreparedStatement createTable() throws Exception {
        return new StatementBuilder(UPDATE_TABLE.getQuery()).build();
    }

    public static PreparedStatement updateName(String uuid, String name) throws Exception {
        return new StatementBuilder(UPDATE_NAME.getQuery()).string(uuid).string(name).string(name).build();
    }

    public static PreparedStatement updateCoins(String uuid, int coins) throws Exception {
        return new StatementBuilder(UPDATE_COINS.getQuery()).integer(coins).string(uuid).build();
    }

    public static PreparedStatement getCoins(String uuid) throws Exception {
        return new StatementBuilder(GET_COINS.getQuery()).string(uuid).build();
    }

}
