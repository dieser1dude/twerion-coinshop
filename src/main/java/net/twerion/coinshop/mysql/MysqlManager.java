package net.twerion.coinshop.mysql;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;
import lombok.Getter;
import net.twerion.coinshop.util.Config;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MysqlManager {

    @Getter private Config config;
    @Getter private Mysql mysql;
    @Getter private LoadingCache<String, Integer> cache;

    private ExecutorService executorService = Executors.newFixedThreadPool(5);

    public MysqlManager(Config config) {
        this.config = config;

        this.mysql = new Mysql(this.config.getString("mysql.host"),
                this.config.getInt("mysql.port"), this.config.getString("mysql.user"),
                this.config.getString("mysql.password"),
                this.config.getString("mysql.database"));

        this.cache = CacheBuilder.newBuilder().expireAfterAccess(1, TimeUnit.MINUTES).build(new MysqlLoader());
    }

    public void executeAsync(Runnable runnable) {
        this.executorService.execute(runnable);
    }

}
