package net.twerion.coinshop.mysql;

import com.google.common.cache.CacheLoader;
import net.twerion.coinshop.Coinshop;
import net.twerion.coinshop.mysql.statement.Statement;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class MysqlLoader extends CacheLoader<String, Integer> {

    @Override
    public Integer load(String uuid) throws Exception {
        PreparedStatement preparedStatement = Statement.getCoins(uuid);
        ResultSet resultSet = Coinshop.getMysqlManager().getMysql().query(preparedStatement);

        int coins = 0;

        if (resultSet.next()) {
            coins = resultSet.getInt("coins");
        }

        preparedStatement.close();
        return coins;
    }

}
