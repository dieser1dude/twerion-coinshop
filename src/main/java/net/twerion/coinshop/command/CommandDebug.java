package net.twerion.coinshop.command;

import lombok.SneakyThrows;
import net.twerion.coinshop.Coinshop;
import net.twerion.coinshop.api.ICoinManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandDebug implements CommandExecutor {

    @SneakyThrows
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        String arg = args[0];
        ICoinManager coinManager = Coinshop.getCoinManager();
        String uuid = ((Player) commandSender).getUniqueId().toString();

        switch (arg) {
            case "current":
                coinManager.loadCoins(uuid, integer -> commandSender.sendMessage("§c" + integer));
                break;
            case "add":
                coinManager.addCoins(uuid, 1);
                break;
            case "remove":
                coinManager.removeCoins(uuid, 1);
                break;
            case "set":
                coinManager.setCoins(uuid, 88);
                break;
        }

        return true;
    }

}
