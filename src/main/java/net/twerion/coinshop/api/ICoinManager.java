package net.twerion.coinshop.api;

import java.util.function.Consumer;

public interface ICoinManager {

    void addCoins(String uuid, int amount);

    void removeCoins(String uuid, int amount);

    void setCoins(String uuid, int coins);

    void loadCoins(String uuid, Consumer<Integer> consumer);

}
