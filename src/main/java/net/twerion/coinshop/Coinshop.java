package net.twerion.coinshop;

import lombok.Getter;
import lombok.SneakyThrows;
import net.twerion.coinshop.api.ICoinManager;
import net.twerion.coinshop.command.CommandDebug;
import net.twerion.coinshop.impl.CoinManager;
import net.twerion.coinshop.listener.PlayerJoin;
import net.twerion.coinshop.mysql.MysqlManager;
import net.twerion.coinshop.mysql.statement.Statement;
import net.twerion.coinshop.util.Config;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.LinkedHashMap;

public class Coinshop extends JavaPlugin {

    @Getter private static MysqlManager mysqlManager;
    @Getter private static ICoinManager coinManager;

    @SneakyThrows
    @Override
    public void onEnable() {
        Config config = new Config("plugins/Coinshop", "config.yml");

        if (!config.contains("mysql")) {
            new LinkedHashMap<String, Object>() {{
                super.put("mysql.host", "127.0.0.1");
                super.put("mysql.port", 3306);
                super.put("mysql.user", "root");
                super.put("mysql.password", "password");
                super.put("mysql.database", "database");
            }}.forEach(config::set);
            config.saveFile();
        }

        Coinshop.mysqlManager = new MysqlManager(config);
        Coinshop.coinManager = new CoinManager(Coinshop.mysqlManager);

        super.getCommand("debug").setExecutor(new CommandDebug());
        super.getServer().getPluginManager().registerEvents(new PlayerJoin(), this);

        Coinshop.mysqlManager.getMysql().openConnection();
        Coinshop.mysqlManager.getMysql().query(Statement.createTable());
    }

    @SneakyThrows
    @Override
    public void onDisable() {
        Coinshop.mysqlManager.getMysql().closeConnection();
    }

}
